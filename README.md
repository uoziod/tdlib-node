# TDLib + NodeJS in Docker

Gitlab pipelines that builds [TDLib + NodeJS](https://hub.docker.com/r/uoziod/tdlib-node) Docker Hub image.
Based on [TDLib in Docker](https://hub.docker.com/r/uoziod/tdlib) ([Gitlab repo](https://gitlab.com/uoziod/tdlib)).

Each branch produces and publishes multi-arch (linux/amd64, linux/arm64 and linux/arm) image with different TDLib and NodeJS version combinations (see [branches section](https://gitlab.com/uoziod/tdlib-node/-/branches/all) or [Docker Hub tags](https://hub.docker.com/r/uoziod/tdlib-node/tags) for available options). Feel free to contribute or reach out if you're looking for some specific combination.

Image tags names (mirroring the repo branches) are formed from `${TDLIB_VERSION}_${NODEJS_VERSION}_${DISTRO}`.


## Usage example

Assuming your app starts with `npm start`:

```dockerfile
FROM uoziod/tdlib-node:1.8.0_18_alpine-3.19


# Cache node_modules

ADD package.json /tmp/package.json
RUN cd /tmp && npm i
RUN mkdir -p /opt/app && cp -a /tmp/node_modules /opt/app/


WORKDIR /opt/app
ADD . /opt/app


CMD ["npm", "start"]
```
